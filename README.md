# TPA container images

This project builds several Debian-based container images for use by TPA and
other Tor Project teams.

## Using the images

### GitLab CI

To use these images to run GitLab CI jobs, use the image's full URL:

    containers.torproject.org/tpo/tpa/base-images/<imagename>:<tag>

For example, to use the Debian stable image:

    containers.torproject.org/tpo/tpa/base-images/debian:stable

To use derivative images such as the Python base image, the Debian release
codename must be used, eg:

    containers.torproject.org/tpo/tpa/base-images/python:bookworm

This is required because the Python interpreter is updated between different
Debian releases, and compatibility can't be guaranteed.

## Build process

### Debian base image

The `debian/build.sh` handles the work of creating, tagging, and pushing the
base Debian images.

These images are built "from scratch" using `mmdebstrap` which creates a
functional system directly from the Debian archive. Some customization and
cleanup is applied to minimize the footprint of the resulting image.

To ensure images are always fresh, they are rebuilt every day from the upstream
archive. If the resulting root filesystem is different, the image is tagged and
uploaded to the registry.  If not, the image is not pushed.

### Derivative images

This project also builds several images with a few extras on top of the base
Debian system:

  * `podman`: rootless podman installation useful for building containers
  * `python`: Python interpreter with the pip and venv modules.

Like the base images, the derivative images are also rebuilt every day to
account for upgrades to the extra packages. In a similar fashion as the base
Debian images, the resulting image digests are compared to the ones currently
in the registry, and are only tagged and pushed when the digest is found to be
different.
