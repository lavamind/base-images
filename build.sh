#!/bin/bash

export TAG_RELEASE="${CI_REGISTRY_IMAGE}/${IMAGE}:${SUITE}"
export TAG_DATE="${TAG_RELEASE}-$(date '+%Y%m%d%H%M')"

podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

podman pull $TAG_RELEASE

export CURRENT_DIGEST=$(podman inspect --type image --format "{{.Id}}" $TAG_RELEASE)
echo "Current registry image digest: ${CURRENT_DIGEST}"

podman build --build-arg SUITE=${SUITE} --tag ${TAG_RELEASE} --tag ${TAG_DATE} --layers=false $IMAGE

export NEW_DIGEST=$(podman inspect --type image --format "{{.Id}}" $TAG_RELEASE)
echo "New image digest: ${NEW_DIGEST}"

echo "Mounting image filesystems..."
export CURRENT_MOUNT=$(podman unshare podman image mount $CURRENT_DIGEST)
export NEW_MOUNT=$(podman unshare podman image mount $NEW_DIGEST)

echo "Comparing images..."
podman unshare bash -c "mtree -k flags,gid,link,mode,nlink,size,type,uid,md5 -X mtree.ignore -cp ${CURRENT_MOUNT}/ | mtree -X mtree.ignore -p ${NEW_MOUNT}/"

if [ $? -eq 0 ]; then
  echo "Newly built image matches digest in current registry image, not pushing."
else
  echo "Pushing new image to registry as ${TAG_RELEASE}"
  podman push ${TAG_RELEASE}
  echo "Pushing new image to registry as ${TAG_DATE}"
  podman push ${TAG_DATE}
fi

exit 0
